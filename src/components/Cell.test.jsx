import React from 'react'
import {
  shallow, configure,
} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'

import Cell from './Cell'

configure({ adapter: new Adapter() })


describe('Test for Cell', () => {
  it('renders column cell with colspan', () => {
    const wrapper = shallow(<Cell colSpan={3}>-</Cell>)
    expect(wrapper.contains(<td colSpan={3}>-</td>)).toBeTruthy()
  })

  it('renders column cell with text', () => {
    const wrapper = shallow(<Cell rowSpan={3}>test</Cell>)
    // Expect the wrapper object to be defined
    expect(wrapper.contains(<td rowSpan={3}>test</td>)).toBeTruthy()
  })
})
