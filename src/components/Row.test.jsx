import React from 'react'
import {
  mount, configure,
} from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Row from './Row'

configure({ adapter: new Adapter() })

describe('Test for Row', () => {
  const row = ['one', 'two', 'three']
  const wrapper = mount(<Row row={row} />)

  it('Row shoul contain 3 column', () => {
    const arr = wrapper.find('Cell')
    expect(arr.length).toEqual(3)
  })
})
