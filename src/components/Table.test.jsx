import React from 'react'
import { mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Table from './Table'

configure({ adapter: new Adapter() })

describe('Test for Table', () => {
  const headers = {
    1: [
      { text: 'Order' },
      { text: 'Task Name' },
      { text: 'Components', colSpan: 2 },
      { text: 'Time', colSpan: 7 },
    ],
  }
  const wrapper = mount(<Table headers={headers} body={[]} />)

  it('Table renders column', () => {
    const arr = wrapper.find('Cell')
    expect(arr.length).toEqual(4)
    expect(arr.first().text()).toEqual('Order')
  })

  it('Table renders Rows', () => {
    const arr = wrapper.find('Row')
    expect(arr.length).toEqual(1)
  })
})
