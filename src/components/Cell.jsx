import React from 'react'
import PropTypes from 'prop-types'
import './Cell.css'

const Cell = (props) => {
  const { colSpan, rowSpan, children } = props
  return (
    <td
      colSpan={colSpan}
      rowSpan={rowSpan}
    >
      {children}
    </td>
  )
}

Cell.propTypes = {
  colSpan: PropTypes.number,
  rowSpan: PropTypes.number,
  children: PropTypes.string,
}

Cell.defaultProps = {
  colSpan: undefined,
  rowSpan: undefined,
  children: '-',
}

export default Cell
