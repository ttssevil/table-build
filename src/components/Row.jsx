import React from 'react'
import PropTypes from 'prop-types'
import Cell from './Cell'

const Row = (props) => {
  const renderCells = (cell, index, maxCount, i) => {
    let colSpan
    if (typeof cell === 'object' && cell && +cell.colSpan > 1) colSpan = +cell.colSpan
    return (
      <Cell
        key={i}
        colSpan={colSpan}
        rowSpan={colSpan > 1 || !maxCount ? undefined : maxCount - index}
      >
        {(typeof cell) === 'object' && cell ? cell.text : cell }
      </Cell>
    )
  }


  const { row, rowsCount, index } = props
  return (
    <tr>
      { row.map((cell, i) => renderCells(cell, index, rowsCount, i)) }
    </tr>
  )
}

Row.propTypes = {
  row: PropTypes.instanceOf(Array).isRequired,
  rowsCount: PropTypes.number,
  index: PropTypes.number,
}

Row.defaultProps = {
  rowsCount: undefined,
  index: undefined,
}

export default Row
