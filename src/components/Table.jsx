import React from 'react'
import PropTypes from 'prop-types'
import Row from './Row'


const Table = (props) => {
  const renderRows = (rows) => {
    const rowsCount = Object.keys(rows).length
    const rowIds = Object.keys(rows)
    return rowIds.map((rowIndex, index) => (
      <Row row={rows[rowIndex]} key={index} index={index} rowsCount={rowsCount} />))
  }

  const renderBody = body => body.map((row, i) => <Row row={row} key={i} />)


  const { headers, body } = props
  return (
    <table style={{ borderSpacing: 0 }}>
      <tbody>
        { renderRows(headers)}
        { renderBody(body)}
      </tbody>
    </table>
  )
}


Table.propTypes = {
  headers: PropTypes.instanceOf(Object).isRequired,
  body: PropTypes.instanceOf(Array).isRequired,
}

export default Table
