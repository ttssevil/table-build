import React, { Component } from 'react'
import Table from './components/Table'


export default class App extends Component {
    state = {
      headers: [],
      body: [],
      rows: {},
    } ;

    componentDidMount() {
      fetch('http://demo4452328.mockable.io/table/1')
        .then(res => res.json())
        .then((result) => {
          this.setState(
            { headers: result.title, body: result.content },
            () => {
              const { headers } = this.state
              this.createHeaders(headers, 0)
            },
          )
        })
    }

    countColSpan = (row, nextRow) => {
      const sum = nextRow.reduce((total, cell) => total + cell.colSpan, 0)
      const diff = row.reduce((total, cell) => (total + cell.colSpan > 1 ? cell.colSpan : 0), 0)
      // if header's colSpan>1 thats mean it also has nodes in next row
      return sum - diff
    }

    createHeaders(headers, row) {
      row += 1
      const { rows } = this.state
      rows[row] = rows[row] || []
      headers.forEach((h) => {
        if (h.children) {
          this.createHeaders(h.children, row)
          rows[row].push({ text: h.value, colSpan: this.countColSpan(rows[row], rows[row + 1]) })
        // colSpan depends on almost rendered next row values
        } else rows[row].push({ text: h.value, colSpan: 1 })
        this.setState({ rows })
      })
    }


    render() {
      const { body, rows } = this.state
      return <Table headers={rows} body={body} />
    }
}
