const HtmlWebPackPlugin = require("html-webpack-plugin");

const htmlPlugin = new HtmlWebPackPlugin({
  template: "./public/index.html",
  filename: "./index.html" , 
  hash: true
});



module.exports = {
    devtool: "source-map",
    module: {
      rules: [
        {
            test: /\.(js|jsx)$/,
            exclude: /node_modules/,
            resolve: {
                extensions: ['.js', '.jsx'],
            } ,
            use: {
                loader: "babel-loader"
            }
        } ,
        {
            test: /\.css$/,
            use: [
              { loader: "style-loader" },
              { loader: "css-loader" }
            ]
          }
      ]
    },
    plugins: [htmlPlugin]
  };

  